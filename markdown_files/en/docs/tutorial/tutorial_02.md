## Static Analysis (Elasticity, Parallel)

The analysis of Section 4.1 was conducted in four parallels using the data of `tutorial/02_elastic_hinge_parallel`.

![Analysis area of each node](./media/tutorial02_01.png){.center width="350px"}

<div style="text-align: center;"> 
Fig. 4.2.1 : Analysis area of each node
</div>
