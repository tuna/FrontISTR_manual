## 静解析（弾性、並列）

4.1節の解析を4並列で実施するには、`tutorial/02_elastic_hinge_parallel` のデータを用います。

![各ノードの計算領域](./media/tutorial02_01.png){.center width="350px"}
<div style="text-align: center;">
図4.2.1 各ノードの計算領域
</div>
